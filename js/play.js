$(document).ready(function() {

  var $lyric = $("#lyric");
  var $explain = $("#explain");
  var numlyrics = lines.length;

  var i=0;
  setTimeout(function(){
    $explain.css('visibility', 'visible');
    update(0);
  }, intervals[0]);

  function update(i)
  {
    $lyric.html("\"" + lines[i] + "\"");
    $explain.html(explanations[i]);
    i++;
    if(i < numlyrics)
    {
      setTimeout(function(){update(i);}, intervals[i]);
    }
  }

});